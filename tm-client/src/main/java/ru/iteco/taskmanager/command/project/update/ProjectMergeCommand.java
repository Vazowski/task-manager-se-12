package ru.iteco.taskmanager.command.project.update;

import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.ReadinessStatus;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class ProjectMergeCommand extends AbstractCommand {

    @Override
    public String command() {
	return "project-persist";
    }

    @Override
    public String description() {
	return "  -  persist project";
    }

    @Override
    public void execute() throws Exception {
	@NotNull
	final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
	@NotNull
	final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpointService().getProjectEndpointPort();
	@Nullable
	final Session session = serviceLocator.getSessionService().getSession();
	if (session == null)
	    return;
	@Nullable
	final User user = userEndpoint.findUserById(session, session.getUserId());
	if (user == null)
	    return;

	System.out.print("Name of project: ");
	@NotNull
	final String inputName = scanner.nextLine();
	System.out.print("Description of project: ");
	@NotNull
	final String inputDescription = scanner.nextLine();
	System.out.print("Date of begining project: ");
	@NotNull
	final String dateBegin = scanner.nextLine();
	System.out.print("Date of ending project: ");
	@NotNull
	final String dateEnd = scanner.nextLine();
	@Nullable
	final Project tempProject = projectEndpoint.findProjectByName(session, inputName);

	@Nullable
	Project project = new Project();
	project.setOwnerId(user.getId());
	project.setName(inputName);
	project.setDescription(inputDescription);
	project.setDateEnd(dateEnd);
	project.setReadinessStatus(ReadinessStatus.PLANNED);

	if (tempProject == null) {
	    String id = UUID.randomUUID().toString();
	    project.setId(id);
	    projectEndpoint.projectMerge(session, project);
	    System.out.println("Done");
	    return;
	}

	if (tempProject.getOwnerId() == user.getId()) {
	    project.setId(tempProject.getId());
	    projectEndpoint.projectMerge(session, project);
	    System.out.println("Done");
	    return;
	}

	System.out.println("Project create other user");
    }
}
