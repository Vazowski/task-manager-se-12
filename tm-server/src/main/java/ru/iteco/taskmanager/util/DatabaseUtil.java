package ru.iteco.taskmanager.util;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DatabaseUtil {

    public static Connection connect() {
	try {
	    final InputStream inputStream = DatabaseUtil.class.getClassLoader().getResourceAsStream("db.properties");
	    final Properties property = new Properties();
	    property.load(inputStream);

	    final String URL = property.getProperty("db.host");
	    final String USER = property.getProperty("db.login");
	    final String PASSWORD = property.getProperty("db.password");

	    return DriverManager.getConnection(URL, USER, PASSWORD);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return null;
    }
}
