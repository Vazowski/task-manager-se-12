package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    @WebMethod
    public void taskMerge(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "task") @Nullable final Task task) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getTaskService().merge(task);
    }

    @WebMethod
    public void taskPersist(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "task") @Nullable final Task task) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getTaskService().persist(task);
    }

    @WebMethod
    public @Nullable Task findTaskById(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "id") @Nullable String id) {
	SignatureUtil.validate(session);
	return serviceLocator.getTaskService().findById(id);
    }

    @WebMethod
    public @Nullable Task findTaskByProjectId(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "projectId") @Nullable String projectId) {
	SignatureUtil.validate(session);
	return serviceLocator.getTaskService().findByProjectId(projectId);
    }

    @WebMethod
    public @Nullable Task findTaskByName(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "taskName") @Nullable String taskName) {
	SignatureUtil.validate(session);
	return serviceLocator.getTaskService().findByName(taskName);
    }

    @WebMethod
    public @Nullable List<Task> findAllTask(@WebParam(name = "session") @Nullable final Session session) {
	SignatureUtil.validate(session);
	return serviceLocator.getTaskService().findAll();
    }

    @WebMethod
    public @Nullable List<Task> findAllTaskByOwnerId(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "ownerId") @Nullable String ownerId) {
	SignatureUtil.validate(session);
	return serviceLocator.getTaskService().findAllByOwnerId(ownerId);
    }

    @WebMethod
    public @Nullable List<Task> findAllTaskByPartOfName(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "partOfName") @Nullable String partOfName) {
	SignatureUtil.validate(session);
	return serviceLocator.getTaskService().findAllByPartOfName(partOfName);
    }

    @WebMethod
    public @Nullable List<Task> findAllTaskByPartOfDescription(
	    @WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "partOfDescription") @Nullable String partOfDescription) {
	SignatureUtil.validate(session);
	return serviceLocator.getTaskService().findAllByPartOfDescription(partOfDescription);
    }

    @WebMethod
    public void removeTaskById(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "id") @Nullable String id) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getTaskService().remove(id);
    }

    @WebMethod
    public void removeAllTask(@WebParam(name = "session") @Nullable final Session session) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getTaskService().removeAll();
    }
}