package ru.iteco.taskmanager.constant;

public final class Literas {

    public static final String ID = "id";
    public static final String OWNER_ID = "userId";
    public static final String PROJECT_ID = "projectId";

    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String FIRST_NAME = "firstName";
    public static final String MIDDLE_NAME = "middleName";
    public static final String LAST_NAME = "lastName";

    public static final String DATE_CREATE = "dateCreate";
    public static final String DATE_BEGIN = "dateBegin";
    public static final String DATE_END = "dateEnd";

    public static final String LOGIN = "login";
    public static final String PASSWORD_HASH = "passwordHash";

    public static final String ROLE_TYPE = "role";
    public static final String READINESS_STATUS = "readinessStatus";

    public static final String TYPE = "type";
}
