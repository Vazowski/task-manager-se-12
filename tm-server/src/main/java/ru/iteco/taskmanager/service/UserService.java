package ru.iteco.taskmanager.service;

import java.sql.Connection;
import java.util.List;

import org.eclipse.persistence.sessions.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.repository.UserRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

public final class UserService extends AbstractService implements IUserService {

    @NotNull
    private UserRepository userRepository;
    @NotNull
    private StringBuilder currentUser = new StringBuilder("root");
    @NotNull
    private Session currentSession;
    @Nullable
    public static Connection connection;

    public UserService() {
	userRepository = new UserRepository();
	connection = DatabaseUtil.connect();
    }

    public void merge(@NotNull final User user) {
	if (user != null)
	    userRepository.merge(user);
    }

    public void persist(@NotNull final User user) {
	if (user != null)
	    userRepository.persist(user);
    }

    @Nullable
    public User findById(@NotNull final String id) {
	if (!id.equals(null))
	    return userRepository.findById(id);
	return null;
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
	if (!login.equals(null))
	    return userRepository.findByLogin(login);
	return null;
    }

    @Nullable
    public List<User> findAll() {
	return userRepository.findAll();
    }

    @Nullable
    public String getCurrent() {
	return currentUser.toString();
    }

    public void setCurrent(@NotNull final String login) {
	if (!login.equals(null))
	    this.currentUser = new StringBuilder(login);
    }
}
