package ru.iteco.taskmanager.service;

import java.sql.Connection;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

public final class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    private ProjectRepository projectRepository;
    @NotNull
    private TaskRepository taskRepository;
    @Nullable
    public static Connection connection;

    public ProjectService(@NotNull final ProjectRepository projectRepository,
	    @NotNull final TaskRepository taskRepository) {
	this.projectRepository = projectRepository;
	this.taskRepository = taskRepository;
	connection = DatabaseUtil.connect();
    }

    public void merge(@NotNull final Project project) {
	if (project != null)
	    projectRepository.merge(project);
    }

    public void persist(@NotNull final Project project) {
	if (project != null)
	    projectRepository.persist(project);
    }

    @Nullable
    public Project findById(@NotNull final String id) {
	if (!id.equals(null)) {
	    return projectRepository.findById(id);
	}
	return null;
    }

    @Nullable
    public Project findByName(@NotNull final String name) {
	if (!name.equals(null)) {
	    return projectRepository.findByName(name);
	}
	return null;
    }

    @Nullable
    public List<Project> findAll() {
	List<Project> list = projectRepository.findAll();
	if (list.size() > 0)
	    return list;
	return null;
    }

    @Nullable
    public List<Project> findAllByOwnerId(@NotNull final String ownerId) {
	List<Project> list = projectRepository.findAllByOwnerId(ownerId);
	if (list.size() > 0)
	    return list;
	return null;
    }

    @Nullable
    public List<Project> findAllByPartOfName(@NotNull final String partOfName) {
	List<Project> list = projectRepository.findAllByPartOfName(partOfName);
	if (list.size() > 0)
	    return list;
	return null;
    }

    @Nullable
    public List<Project> findAllByPartOfDescription(@NotNull final String parOfDescription) {
	List<Project> list = projectRepository.findAllByPartOfDescription(parOfDescription);
	if (list.size() > 0)
	    return list;
	return null;
    }

    public void remove(@NotNull final String id) {
	if (!id.equals(null)) {
	    taskRepository.remove(id);
	    projectRepository.remove(id);
	}
    }

    public void removeAll() {
	taskRepository.removeAll();
	projectRepository.removeAll();
    }
}
