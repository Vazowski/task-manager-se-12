package ru.iteco.taskmanager.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import ru.iteco.taskmanager.api.repository.ITaskRepository;
import ru.iteco.taskmanager.constant.Literas;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.enumerate.ReadinessStatus;
import ru.iteco.taskmanager.service.TaskService;

@NoArgsConstructor
public final class TaskRepository implements ITaskRepository {

    @SneakyThrows
    public void merge(@NotNull final Task task) {
	@NotNull
	final String query = "UPDATE app_task SET " + Literas.OWNER_ID + " = ?, " + Literas.PROJECT_ID + " = ?, "
		+ Literas.NAME + " = ?, " + Literas.DESCRIPTION + " = ?, " + Literas.DATE_CREATE + " = ?, "
		+ Literas.DATE_BEGIN + " = ?, " + Literas.DATE_END + " = ?, " + Literas.TYPE + " = ?, "
		+ Literas.READINESS_STATUS + " = ?, " + "WHERE " + Literas.ID + " = ?";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setObject(1, task.getOwnerId());
	statement.setObject(2, task.getProjectId());
	statement.setString(3, task.getName());
	statement.setString(4, task.getDescription());
	statement.setObject(5, task.getDateCreated());
	statement.setObject(6, task.getDateBegin());
	statement.setObject(7, task.getDateEnd());
	statement.setString(8, task.getType());
	statement.setString(9, task.getReadinessStatus().getDisplayName());
	statement.setString(10, task.getId());
	statement.executeUpdate();
	statement.close();
    }

    @SneakyThrows
    public void persist(@NotNull final Task task) {
	@NotNull
	final String query = "INSERT INTO app_task (" + Literas.ID + ", " + Literas.OWNER_ID + ", " + Literas.PROJECT_ID
		+ ", " + Literas.NAME + ", " + Literas.DESCRIPTION + ", " + Literas.DATE_CREATE + ", "
		+ Literas.DATE_BEGIN + ", " + Literas.DATE_END + ", " + Literas.TYPE + ", " + Literas.READINESS_STATUS
		+ ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, task.getId());
	statement.setObject(2, task.getOwnerId());
	statement.setObject(3, task.getProjectId());
	statement.setString(4, task.getName());
	statement.setString(5, task.getDescription());
	statement.setObject(6, task.getDateCreated());
	statement.setObject(7, task.getDateBegin());
	statement.setObject(8, task.getDateEnd());
	statement.setString(9, task.getType());
	statement.setString(10, task.getReadinessStatus().getDisplayName());
	statement.executeUpdate();
	statement.close();
    }

    @Nullable
    @SneakyThrows
    public Task findById(@NotNull final String id) {
	@NotNull
	final String query = "SELECT * FROM app_task WHERE " + Literas.ID + " = ?";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return null;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, id);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	if (!resultSet.next())
	    return null;
	@Nullable
	final Task task = fetch(resultSet);
	if (task != null)
	    return task;
	statement.close();
	return null;
    }

    @Nullable
    @SneakyThrows
    public Task findByProjectId(@NotNull final String projectId) {
	@NotNull
	final String query = "SELECT * FROM app_task WHERE " + Literas.PROJECT_ID + " = ?";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return null;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, projectId);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	if (!resultSet.next())
	    return null;
	@Nullable
	final Task task = fetch(resultSet);
	if (task != null)
	    return task;
	statement.close();
	return null;
    }

    @Nullable
    @SneakyThrows
    public Task findByName(@NotNull final String name) {
	@NotNull
	final String query = "SELECT * FROM app_task WHERE " + Literas.NAME + " = ?";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return null;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, name);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	if (!resultSet.next())
	    return null;
	@Nullable
	final Task task = fetch(resultSet);
	if (task != null)
	    return task;
	statement.close();
	return null;
    }

    @Nullable
    @SneakyThrows
    public List<Task> findAll() {
	@NotNull
	final String query = "SELECT * FROM app_task";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<Task> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @Nullable
    @SneakyThrows
    public List<Task> findAllByOwnerId(@NotNull final String ownerId) {
	@NotNull
	final String query = "SELECT * FROM app_task WHERE " + Literas.OWNER_ID + " = ?";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, ownerId);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<Task> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @Nullable
    @SneakyThrows
    public List<Task> findAllByPartOfName(@NotNull final String partOfName) {
	@NotNull
	final String query = "SELECT * FROM app_task WHERE " + Literas.NAME + " LIKE '%?%'";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, partOfName);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<Task> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @Nullable
    @SneakyThrows
    public List<Task> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	@NotNull
	final String query = "SELECT * FROM app_task WHERE " + Literas.DESCRIPTION + " LIKE '%?%'";

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, partOfDescription);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<Task> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @SneakyThrows
    public void remove(@NotNull final String id) {
	@NotNull
	final String query = "DELETE FROM app_task WHERE " + Literas.ID + " = " + id;

	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.executeUpdate();
	statement.close();
    }

    @SneakyThrows
    public void removeAll() {
	@NotNull
	final String query = "DELETE * FROM app_task";
	@Nullable
	final Connection connection = TaskService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.executeUpdate();
	statement.close();
    }

    @Nullable
    @SneakyThrows
    private Task fetch(@Nullable final ResultSet row) {
	if (row == null)
	    return null;
	@NotNull
	final Task task = new Task();

	task.setId(row.getString(Literas.ID));
	task.setOwnerId(row.getString(Literas.OWNER_ID));
	task.setProjectId(row.getString(Literas.PROJECT_ID));
	task.setName(row.getString(Literas.NAME));
	task.setDescription(row.getString(Literas.DESCRIPTION));
	task.setDateCreated(row.getString(Literas.DATE_CREATE));
	task.setDateBegin(row.getString(Literas.DATE_BEGIN));
	task.setDateEnd(row.getString(Literas.DATE_END));
	task.setType("Project");

	@Nullable
	final String readinessStatus = row.getString(Literas.READINESS_STATUS);
	if (readinessStatus != null) {
	    switch (readinessStatus) {
	    case ("DURING"):
		task.setReadinessStatus(ReadinessStatus.DURING);
		break;
	    case ("PLANNED"):
		task.setReadinessStatus(ReadinessStatus.PLANNED);
		break;
	    case ("READY"):
		task.setReadinessStatus(ReadinessStatus.READY);
		break;
	    }
	}
	return task;
    }
}
